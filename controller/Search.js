const express = require("express");
const router = express.Router();
const ProductM = require("../model/Product");
const CatM = require('../model/Category');
router.post('/', async(req, res) => {
    let query = req.body.query; 
    console.log("xxx");
    console.log(req.body);
    let searchType =  req.body.searchTypeValue;
    if (typeof query == undefined) {
        res.redirect('/');
    } else {
        res.redirect(`/search/${searchType}/${query}?page=1&perpage=5`);

    }
});
router.get("/", async(req, res)=> {
    res.redirect('/');
});
router.get('/:searchType/:query', async (req,res) => {
    let searchType = req.params.searchType;
    const query = req.params.query;
    let page = req.query.page;
    let perpage = req.query.perpage;
    console.log('query ' + query)
    console.log('sss' + searchType);
    
  //  res.end('{"success" : "Updated Successfully", "status" : 200}');
      

    if (typeof query == undefined) {
        res.redirect('/');
    } else {
        let rs;
        const cat = await CatM.getCatAll();
        if(searchType == '-1'){
            rs = await ProductM.searchByName(query);
           
           
        }
        else{
           rs = await ProductM.searchByNameAndCat(query,searchType);
            console.log(rs);
                
            // if (req.session.user) {
            //     res.render("home", {
            //         layout: "searchLayout.hbs",
            //         notLoggedIn: false,
            //         item: rs,  
            //         query: query,
            //         NameCustomer: req.session.user.UserName,
            //         cats :cat
            //     });
            // } else {
            //     res.render("home", {
            //         layout: "searchLayout.hbs",
            //         notLoggedIn: true,
            //         item: rs,
            //         query: query,
            //         cats :cat
            //     });
            // }
        }
        let totalpage = Math.ceil(rs.length/perpage);
        console.log(totalpage);
        console.log(page);
        let listpage = [];

        totalpage = parseInt(totalpage);
        page = parseInt(page);
        if (page-2 > 0)
        listpage.push(x = {page: page - 2, isActive: false, query: query, searchType: searchType, perpage: perpage});
    
        if (page-1 > 0) listpage.push(x = {page: page - 1, isActive: false, query: query, searchType: searchType, perpage: perpage});
        listpage.push(x = {page: page , isActive: true, query: query, searchType: searchType, perpage: perpage});
        if (page + 1 <= totalpage) listpage.push(x = {page: page + 1, isActive: false, query: query, searchType: searchType, perpage: perpage});
        if (page + 2 <= totalpage) listpage.push(x = {page: page + 2, isActive: false, query: query, searchType: searchType, perpage: perpage});
        console.log(listpage);
        rs = rs.slice((page-1)*perpage, page*perpage);
        if (req.session.user) {
            res.render("home", {
                layout: "searchLayout.hbs",
                notLoggedIn: false,
                item: rs, 
                searchType: searchType, 
                query: query,
                listpage: listpage,
                NameCustomer: req.session.user.UserName,
                cats :cat,
                perpage:perpage,
                totalpage: totalpage,
            });
        } else {
            res.render("home", {
                layout: "searchLayout.hbs",
                notLoggedIn: true,
                item: rs,
                searchType: searchType, 
                query: query,
                perpage:perpage,
                cats :cat,
                page:page,
                totalpage: totalpage,
                listpage: listpage
            });
        }
       
    }
});

router.get("/:searchType/:query/:mode", async(req, res) => {
    const mode = req.params.mode;
    const query = req.params.query;
    const searchType = req.params.searchType;
    const page = req.query.page;
    const perpage = req.query.perpage;
    console.log("mode: ", mode);
    const rs = await ProductM.searchByNameWithSort(query, mode);
    const cats = await CatM.getCatAll();
    let totalpage = 1;
    let item =rs;
    if (rs != null) {
        totalpage = Math.ceil(rs.length/perpage);
        item = rs.slice((page - 1) * perpage, page *perpage);
    }
    if (req.session.user) {
        res.render("home", {
            layout: "searchLayout.hbs",
            notLoggedIn: false,
            item: item,
            cats: cats,
            query: query,
            searchType: searchType,
            NameCustomer: req.session.user.UserName
          });
    } else {
        res.render("home", {
            layout: "searchLayout.hbs",
            notLoggedIn: true,
            item: item,
            query: query,
            searchType: searchType,
            cats: cats,
          });
    }
});

module.exports = router;