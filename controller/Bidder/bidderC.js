const express = require("express");
const bodyParser = require("body-parser");
var nodemailer = require("nodemailer");
const router = express.Router();
const ProductMoB = require("../../model/Bidder/Product");
const ProductM = require("../../model/Product");
const CatM = require("../../model/Category");
const mBidPerInfo=require("../../model/Bidder/User")
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.get("/watch-list", async (req, res) => {
  const list = await ProductMoB.getListWatchListItem(req.session.user.Id);
  const cats = await CatM.getCatAll();
  // let listItem = [];
  // if (list != null) {
  //     for (let i = 0; i < list.length; i++) {
  //         let item = await ProductM.getInfoProducts(list[0].ID_SPYT);
  //         listItem.push(item );
  //     }
  // }
  //console.log(listItem);
  res.render("home", {
    layout: "watchlistlayout.hbs",
    notLoggedIn: false,
    watchList: list,
    NameCustomer: req.session.user.UserName,
    cats: cats
  });
});


var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: "mrngunuong1@gmail.com",
    pass: "bacsi1212"
  }
});

router.post("/daugia", async (req, res) => {
  let cost = req.body.data;
  let id = req.body.idPro;
  let product = await ProductM.getInfoProducts(id);
  let hBidder = await ProductM.getBidderHighestPrice(id);
  let response;
  if (cost >= hBidder[0].Price + product[0].Price_Step) {
    let rs = await ProductMoB.dauGia(req.session.user.Id, id, cost);
    console.log(rs);
    response = {
      isSuccess: 1
    };
    console.log(req.session.user);
    let mailOptions;
    mailOptions = {
      to: req.session.user.Email,
      subject: "SanDauGia - Ra giá thành công",
      html: `Hello ${req.session.user.UserName}, <br> Bạn vừa ra giá cho sản phẩm ${product[0].Name_Product} với giá là ${cost} VND thành công.<br>`
    };
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response) {
      if (error) {
        console.log("Error while sending OTP: ", error);
        res.end("error");
      } else {
        console.log("Message sent: " + response.message);
        res.end("sent");
      }
    });
  } else {
    response = {
      isSuccess: 0
    };
  }

  console.log(JSON.stringify(response));
  res.end(JSON.stringify(response));
});
router.get('/sendResult', (req,res) =>{
    
});



router.get('/profile', async(req,res)=>{
  const id = req.session.user.Id;
  console.log("Enter profile");
  const personalInfo = await mBidPerInfo.byID(id);
  const profile = personalInfo[0];        //
  if (req.query['status'] == null){
      req.query['status'] = 'disabled';
  }
  
  var btn1 = "";
  var btn2 = "";
  //console.log('day la req ' + req.query['status']);
  if (req.query['status'] == 'disabled' ){    
      btn1 = '<a href="/user/profile?status=enable"> <button type="button"  class="btn btn-primary">Chỉnh sửa</button> </a>';
      btn2 = '<a href="/user/profile/changepassword"> <button type="button"  class="btn btn-danger">Đổi mật khẩu</button> </a>';
  }
  else{  
      btn1 = '<button name="save" type="submit"  class="btn btn-primary" formaction = "profile/changedSubmit"  > Save</button>';
      btn2 = '<button name="quayLai" type="button" class="btn btn-secondary" value = "Quay về" onClick = onConfirmQuayVe()>Quay về</button>';
  }
 
  // LAY DANH GIA
  const danhGia = await mBidPerInfo.getAllDanhGia(id);
  // LAY SAN PHAM DANG THAM GIA DAU GIA
  const dealingProduct = await mBidPerInfo.getDealingProduct(id); 
  // LAY SAN PHAM DA THANG
  const winningProduct = await mBidPerInfo.getWinningProduct(id);
  console.log(danhGia);
  res.render('profile.hbs',{
    layout:'mainProfile.hbs',
      title: 'Thông tin',
      status: req.query['status'],
      btn1: btn1,
      btn2: btn2,
      profile : profile,
      danhGia: danhGia,
      dealingProduct: dealingProduct,
      winningProduct: winningProduct,
      NameCustomer: req.session.user.UserName,
      notLoggedIn: false,
      
  });

});

router.post('/profile/changedSubmit', (req,res) => {
  const id = req.session.user.Id;
  const hoTen = req.body.hoTen;
  const diaChi = req.body.diaChi;
  const sdt = req.body.sdt;
  const email = req.body.email;
  console.log(req.body);
  mBidPerInfo.updateProfile(id, hoTen,diaChi,sdt,email);
  res.redirect(`/user/profile`);


});
//changerpassword
router.get('/profile/changepassword', (req,res) =>{
  const id = req.session.user.Id;
  let btn1 = "";
  let btn2 = "";

  btn1 = '<button name="save" type="submit"  class="btn btn-primary">Save</button>';
  btn2 = '<button name="quayLai" type="button" class="btn btn-secondary" value = "Quay về" onClick = onConfirmQuayVe()>Quay về</button>';
  
  res.render('changepassword.hbs',{
    layout:'mainProfile.hbs',
      title: 'Change password',
      btn1: btn1,
      btn2: btn2,
      NameCustomer: req.session.user.UserName,
      notLoggedIn: false,
  });
});
router.get('/profile/promote',(req,res)=>{
  const id = req.session.user.Id;

  mBidPerInfo.promote(id);
  res.render('promote',{
    layout:'mainProfile.hbs',
      title: 'Xin bán 7 ngày',
      noti: 'Yêu cầu bán đang chờ được xử lí. Xin cảm ơn!',
      NameCustomer: req.session.user.UserName,
      notLoggedIn: false,
  });
});
module.exports = router;
