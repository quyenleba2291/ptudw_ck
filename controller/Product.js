const express = require("express");
var bodyParser = require("body-parser");
const router = express.Router();
const ProductM = require("../model/Product");
const CatM = require("../model/Category");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.get("/:id/", async (req, res) => {
  const id = parseInt(req.params.id);
  console.log(id);
  const product = await ProductM.getInfoProducts(id);
  const seller = await ProductM.getInfoSellerOfProduct(
    product[0].Id_Seller_Information
  );
  const cat = await CatM.getCatAll();
  product[0].Name_Seller = seller[0].Name_Customer;
  const bidders = await ProductM.getBidder(id);
  console.log(bidders);
  let bidders_json = JSON.parse(JSON.stringify(bidders));
  String.prototype.replaceAt = function(index, replacement) {
    return (
      this.substr(0, index) +
      replacement +
      this.substr(index + replacement.length)
    );
  };
  let minCost = product[0].Price;
  product[0].minCost = minCost;
  convertTime = function(dateSrc) {
    date = new Date(dateSrc);
      year = date.getFullYear();
      month = date.getMonth() + 1;
      dt = date.getDate();
      h = date.getHours();
      m = date.getMinutes();
      s = date.getSeconds();
      if (dt < 10) {
        dt = "0" + dt;
      }
      if (month < 10) {
        month = "0" + month;
      }
      if (m < 10) m = "0" + m;
      if (h < 10) h = "0" + h;
      if (s < 10) s = "0" + s;
  
      return dt + "/" + month + "/" + year + " " + h + ":" + m + ":" + s;
  }
  product[0].Time_Submitted_Product = convertTime(product[0].Time_Submitted_Product);
  product[0].Time_End_Submitted = convertTime(product[0].Time_End_Submitted);
  if (bidders_json != null) {
    for (let i = 0; i < bidders_json.length; i++) {
      bidders_json[i].index = i + 1;
      let str = bidders_json[i].Name_Customer;
      if (str.length < 5) str = "*****";
      else
        for (let i = 0; i < str.length - 3; i++) {
          if (str[i] != " ") str = str.replaceAt(i, "*");
        }
      bidders_json[i].Name_Customer = str;
      date = convertTime(bidders_json[i].Date_Of_Sale);
      bidders_json[i].Date_Of_Sale = date;
    }
    minCost = bidders_json[0].Price + product[0].Price_Step;
    product[0].minCost = bidders_json[0].Price; 
  }
  let allBidder = await ProductM.getAllBidder(id);
  let allBidder_json = JSON.parse(JSON.stringify(allBidder));
  if(allBidder != null) {
    for (let i = 0; i < allBidder.length; i++) {
      date = convertTime(allBidder[i].Date_Of_Sale);
      allBidder[i].Date_Of_Sale = date;
    }
  }

  console.log(minCost);
  if (req.session.user) {
    const isLiked = await ProductM.isLikeProduct(req.session.user.Id, id);
    console.log(isLiked);
    product[0].isLike = isLiked;
    res.render("home", {
      layout: "productDetail.hbs",
      notLoggedIn: false,
      title: `${product[0].Name_Product}`,
      item: product,
      cats: cat,
      NameCustomer: req.session.user.UserName,
      bidder: bidders_json,
      minCost: minCost,
      allBidder: allBidder
    });
  } else {
    res.render("home", {
      layout: "productDetail.hbs",
      title: `${product[0].Name_Product}`,
      item: product,
      cats: cat,
      notLoggedIn: true,
      isLike: false,
      bidder: bidders_json,
      minCost: minCost,
      allBidder: allBidder
    });
  }
});
router.get("/:id/like", async (req, res) => {
  const id = parseInt(req.params.id);
  console.log("like: ", id);
  if (req.session.user) {
    //do like or unlike
    await ProductM.likeAProduct(req.session.user.Id, id);
    res.redirect(`/product/${id}`);
  } else {
    res.redirect(`/product/${id}`);
  }
});
router.post('/allBidder', async(req,res)=> {
  console.log("all");
  let id = req.body.idPro;
  console.log(id);
  const rs = await ProductM.getAllBidder(id);
  console.log(JSON.stringify(rs));
  res.end(JSON.stringify(rs));
})
module.exports = router;
