var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log("get home page")
  res.render('home', { layout: 'layout.hbs' });
});
router.get('/register',function(req, res, next){
  res.render('home',{layout:'register.hbs'});
})
router.get('/signout', function(req, res, next){
  res.redirect('/');
})

module.exports = router;
