const express = require("express");
const router = express.Router();
const CatM = require("../model/Category");
const ProductM = require("../model/Product");

router.get("/", async (req, res) => {
  res.redirect("/");
});
router.get("/:id/", async (req, res) => {
  const id = parseInt(req.params.id);
  const cats = await CatM.getCatAll();
  const cat = await CatM.getCatById(id);

  let page = req.query.page;
  let perpage = req.query.perpage;
  

  const items = await ProductM.getProByCat(id);
  const cats_s = [];
  console.log(cats);
  for (let cat_i of cats) {
      let str = JSON.parse(JSON.stringify(cat_i));
    str.isActive = false;
    if (str.Id_Category_Lv1 == id) {
        str.isActive = true;
    }
    cats_s.push(str);
  };
  console.log(page);
  console.log(perpage);
  let totalpage = Math.ceil(items.length / perpage);
  totalpage = parseInt(totalpage);
  page = parseInt(page);
  let listpage = [];
  if (page-2 > 0)
  listpage.push(x = {page: page - 2, isActive: false, Id_Category_Lv1: id,  perpage: perpage});

  if (page-1 > 0) listpage.push(x = {page: page - 1, isActive: false, Id_Category_Lv1: id,  perpage: perpage});
  listpage.push(x = {page: page , isActive: true, Id_Category_Lv1: id,  perpage: perpage});
  if (page + 1 <= totalpage) listpage.push(x = {page: page + 1, isActive: false, Id_Category_Lv1: id,  perpage: perpage});
  if (page + 2 <= totalpage) listpage.push(x = {page: page + 2, isActive: false, Id_Category_Lv1: id,  perpage: perpage});
  console.log(listpage);
  let rs = items.slice((page-1) * perpage, page*perpage);
  console.log(rs);
  if (req.secure.user) {
    res.render("home", {
      layout: "catLayout.hbs",
      cat: cat,
      cats: cats_s,
      notLoggedIn: false,
      item: rs,
      listpage: listpage,
      perpage:perpage,
      totalpage: totalpage,
      Id_Category_Lv1: id,
      NameCustomer: req.session.user.UserName
    });
  } else {
    res.render("home", {
      layout: "catLayout.hbs",
      cat: cat,
      cats: cats_s,
      item: rs,
      notLoggedIn: true,
      listpage: listpage,
      perpage:perpage,
      totalpage: totalpage,
      Id_Category_Lv1: id,
    });
  }
});
module.exports = router;
