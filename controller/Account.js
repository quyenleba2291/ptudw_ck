const express = require("express");
const router = express.Router();
const sha = require("sha.js");
const accountM = require("../model/Account");
const productM = require("../model/Product");
const CatM = require("../model/Category");
var nodemailer = require("nodemailer");
const hashLength = 64;
router.get("/", async (req, res) => {
  if (req.session.user) {
    if (
      req.session.user.Emailconfirmed === false ||
      req.session.user.Emailconfirmed === 0
    ) {
      res.redirect("/checkOTP");
    } else {
      console.log("vo bidder");
      let listEnd = await productM.getEndProducts();
      let listHigh = await productM.getHighestPriceProducts();
      let topTrend = await productM.getTrendingProducts();
      const cat = await CatM.getCatAll();
      res.render("home", {
        layout: "layout.hbs",
        notLoggedIn: false,
        endTime: listEnd,
        highestPrice: listHigh,
        topTrend: topTrend,
        cats: cat,
        isSeller: req.session.user.isSeller,
        NameCustomer: req.session.user.UserName
      });
    }
  } else {
    let listEnd = await productM.getEndProducts();
    let listHigh = await productM.getHighestPriceProducts();
    let topTrend = await productM.getTrendingProducts();
    const cat = await CatM.getCatAll();
    console.log(listHigh);
    res.render("home", {
      layout: "layout.hbs",
      notLoggedIn: true,
      endTime: listEnd,
      highestPrice: listHigh,
      topTrend: topTrend,
      cats: cat
    });
  }
});
router.get("/login", async (req, res) => {
  res.render("home", { layout: "login.hbs" });
});

router.get("/register", async (req, res) => {
  if (!req.session.user || typeof req.session.user === undefined) {
    res.render("home", { layout: "register.hbs" });
  } else {
    res.redirect("/sendOTP");
  }
});

router.get("profile", (req, res) => {
  if (!req.session.user) {
    res.redirect("/login");
  }
  res.redirect("/");
});

router.post("/login", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const user = await accountM.getByUsername(username);
  if (user === null) {
    res.redirect("/");
    return;
  }
  console.log(user);
  const pwDb = user.PasswordHash;
  const salt = pwDb.substring(hashLength, pwDb.hashLength);
  const preHash = password + salt;
  const hash = sha("sha256")
    .update(preHash)
    .digest("hex");
  const pwHash = hash + salt;

  if (pwHash === pwDb) {
    req.session.user = user;
    console.log("đã đăng nhập");
    const id = req.session.user.Id;
    
    const rows = await accountM.getSellerById(id);
    if (rows != null) {
      req.session.user.isSeller = true;
    } else {
      req.session.user.isSeller = false;
    }
    if (
      req.session.user.Emailconfirmed === 0 ||
      req.session.user.Emailconfirmed == false
    ) {
      res.redirect("/sendOTP");
    } else {
      res.redirect("/");
    }
  } else {
    res.send("Sai mat khau");
  }
});
var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: "mrngunuong1@gmail.com",
    pass: "bacsi1212"
  }
});
router.post("/register", async (req, res, next) => {
  if (!req.session.user) {
    const username = req.body.username;
    const password = req.body.password;

    const salt = Date.now().toString(16);
    const preHash = password + salt;
    const hash = sha("sha256")
      .update(preHash)
      .digest("hex");
    const pwHash = hash + salt;

    let user = {
      Id: "3",
      Email: req.body.email,
      Emailconfirmed: 0,
      PasswordHash: pwHash,
      PhoneNumber: "123",
      UserName: username,
      Name: req.body.fullname,
      Address: req.body.address,
      Phone: req.body.phone
    };
    console.log(user);
    let isExistedEmail = await accountM.getByEmail(user.Email);
    let isExistedUsername = await accountM.getByUsername(user.UserName);
    let notMatch = false;
    if (isExistedEmail !== null) {
      isExistedEmail = true;
    }
    if (isExistedUsername !== null) {
      isExistedUsername = true;
    }
    if (req.body.password !== req.body.confirm_password) {
      notMatch = true;
    }

    //reCaptcha
    // if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
    //   return res.json({"responseCode" : 1,"responseDesc" : "Please select captcha"});
    // }
    // Put your secret key here.
    var secretKey = "6LdhNMgUAAAAACeXTJ2nOuTDYe4y-P1Z0P7LsEnj";
    // req.connection.remoteAddress will provide IP address of connected user.
    var verificationUrl =
      "https://www.google.com/recaptcha/api/siteverify?secret=" +
      secretKey +
      "&response=" +
      req.body["g-recaptcha-response"] +
      "&remoteip=" +
      req.connection.remoteAddress;
    // Hitting GET request to the URL, Google will respond with success or error scenario.
    //Error - invalid key type
    // request(verificationUrl, function(error,response,body) {
    //   body = JSON.parse(body);
    //   // Success will be true or false depending upon captcha validation.
    //   if(body.success !== undefined && !body.success) {
    //     return res.json({"responseCode" : 1,"responseDesc" : "Failed captcha verification"});
    //   }
    //   return res.json({"responseCode" : 1,"responseDesc" : "Failed captcha verification"});
    // });
    if (isExistedEmail || isExistedUsername || notMatch) {
      res.render("home", {
        layout: "register.hbs",
        errorEmail: isExistedEmail,
        errorUsername: isExistedUsername,
        errorConfirm: notMatch
      });
    } else {
      const uId = await accountM.add(user);
      console.log("uId: " + uId);
      let user_created = await accountM.getByUsername(user.UserName);
      user.Id = user_created.Id;
      console.log(user);
      const memberId = await accountM.addCustomer(user);
      console.log("memberId" + memberId);
      req.session.user = user;

      res.redirect("/sendOTP");
    }
  } else {
    res.redirect("/");
  }
});
router.get("/sendOTP", async (req, res) => {
  if (
    (req.session.user && req.session.user.Emailconfirmed === false) ||
    req.session.user.Emailconfirmed == 0
  ) {
    host = req.get("host");
    link =
      "http://" +
      req.get("host") +
      "/verify?id=" +
      req.session.user.PasswordHash;
    mailOptions = {
      to: req.session.user.Email,
      subject: "SanDauGia - Please confirm OTP Code",
      html:
        "Hello,<br> Please Click on the link to verify your email.<br><a href=" +
        link +
        ">Click here to verify</a>"
    };
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response) {
      if (error) {
        console.log("Error while sending OTP: ", error);
      } else {
        console.log("Message sent: " + response.message);
      }
    });
    res.redirect("/checkOTP");
  } else {
    res.redirect("/");
  }
});
router.get("/checkOTP", async (req, res) => {
  if (req.session.user) {
    if (
      req.session.user.Emailconfirmed === false ||
      req.session.user.Emailconfirmed == 0
    ) {
      res.render("home.hbs", { layout: "checkOTP.hbs" });
      return;
    } else {
      res.redirect("/");
    }
  } else {
    res.redirect("/");
  }
});

router.get("/signout", async (req, res) => {
  console.log("da xóa data");
  req.session.user = null;
  res.redirect("/");
});
router.use("/user", async(req,res,next) => {
  if (req.session.user) {
    console.log("avsa");
    next();
  } else {
    res.redirect('/');
  }
});
router.use('/seller', async(req, res, next) => {
  console.log("check seller");
  if (req.session.user) {
    const id = req.session.user.Id;
    const rows = await accountM.getSellerById(id);
    if (rows != null) {
      req.session.user.isAdmin = true;
      next();
    } else{
      req.session.user.isAdmin = false;
      res.redirect('/');
    }
  } else {
    res.redirect('/');
  }

});
module.exports = router;
