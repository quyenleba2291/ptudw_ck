const db = require('../utils/db');
const tbName = 'User_Tab';

module.exports = {
    add: async user => {
        const id = await db.add(tbName,user);
        return id;
    },
    addCustomer: async user => {
        const id = await db.addCustomer(tbName, user);
        return id;
    },
    updateEmailConfirm: async email => {
        let sql = `UPDATE ${tbName} SET Emailconfirmed = 1 where Email = '${email}'`;
        console.log("Sql: ",sql);
        const rs = await db.load(sql);
    },
    getByUsername: async username => {
        let sql = `SELECT * FROM ${tbName} WHERE  UserName = '${username}'`;
        console.log("accountM/getByUsername: sql = ", sql);
        const rs = await db.load(sql);
        if (rs.length > 0) {
            return rs[0];
        }
        return null;
    },
    getByHash: async hash => {
        let sql = `SELECT * FROM ${tbName} WHERE PasswordHash = '${hash}'`
        const rs = await db.load(sql);
        if (rs.length > 0) {
            return rs[0];
        }
        return null;
    },
    getByEmail: async email => {
        let sql = `SELECT * FROM ${tbName} WHERE Email = '${email}'`;
        const rs = await db.load(sql);
        if (rs.length > 0) {
            return rs[0];
        }
        return null;
    },
    getSellerById:  async(ID) => {
        const sql =`SELECT * FROM Customer WHERE Id_Customer = ${ID} and Id_Kind_Of_Custom = 3`;
        const rows = await db.load(sql);
        if (rows.length > 0) {
            return rows;
        } else return null;
    },
};