const db = require("../utils/db");


module.exports = {
    getCatAll: async() => {
        const sql = `SELECT * FROM Categorylv1`;
        const rs = await db.load(sql);
        if (rs.length > 0) {
            return rs;
        } 
        return null;
    },
    getCatById: async id => {
        const sql = `SELECT * FROM CategoryLv1 WHERE Id_Category_Lv1 = ${id}`;
        const rs = await db.load(sql);
        if (rs.length > 0) {
            return rs;
        } 
        return null;
    },
};
