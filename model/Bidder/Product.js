const db = require("../../utils/db");

module.exports = {
  getListWatchListItem: async idUser => {
    const sql = `SELECT * FROM Watch_List, Product WHERE Product.Id_Product = Watch_List.ID_SPYT and Id_Customer = ${idUser}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  dauGia: async (idUser, idPro, cost) => {
    const sql = `INSERT INTO History_Deal(Id_Product, Id_Buyer, Invalid, Date_Of_Sale, Price) values
                (${idPro}, ${idUser}, 1, NOW(), ${cost})`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  }
};
