const db = require('../../utils/db');
const dbName = 'Data_Web';
const tbCustomer = 'Customer';
const tbEvaluate_customer = 'Evaluate_Customer';
const tbHistory_deal = 'History_Deal';
const tbProduct = 'product';
const tbProduct_buyer = 'Product_Buyer';
const tbWatch_List = 'Watch_List';
module.exports = {
    all: async() => {
        const sql =`SELECT * FROM ${tbCustomer}`;   
        const rows = await db.load(sql);
        return rows;
    },
    byID:  async(ID) => {
        const sql =`SELECT * FROM ${tbCustomer} WHERE Id_Customer = ${ID}`;
        const rows = await db.load(sql);
        return rows;
    },
    updateProfile: async(id,hoTen, diaChi, sdt, email)=>{
        const sql = `UPDATE ${tbCustomer} SET Name_Customer = '${hoTen}', Address_Customer = '${diaChi}', Phone = '${sdt}', Email = '${email}'
                     WHERE Id_Customer = ${id}`;
        await db.load(sql);
        return 1;         
    },
    getAllDanhGia: async(id)=>{
    
        const sql = `SELECT * FROM ${tbEvaluate_customer}, Customer WHERE ${tbEvaluate_customer}.Id_Customer = ${id} and ${tbEvaluate_customer}.Id_Man = Customer.Id_Customer`;
        const rows = await db.load(sql);
        return rows;
    },
    getDealingProduct: async(id)=>{
        const sql = `SELECT * FROM ${tbHistory_deal}, Customer,Product WHERE ${tbHistory_deal}.Id_Buyer = ${id} and ${tbHistory_deal}.Id_Seller = Customer.Id_Customer and Product.Id_Product = ${tbHistory_deal}.Id_Product`;
        
        const rows = await db.load(sql);
        return rows;
    },
    getWinningProduct: async(id)=>{
        const sql = `SELECT * FROM ${tbProduct_buyer} AS B, ${tbProduct} AS P WHERE B.Id_product = P.Id_product AND B.Id_buyer = ${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    promote: async(id)=>{
        const sql = `INSERT INTO promote (Id_customer,_comment,Invalid) VALUES (${id},'xin phép được bán sản phẩm', 0)`;
        await db.load(sql);
        return 1;
    },
    getWatchList: async(id)=>{
        const sql = `SELECT * FROM ${tbWatch_List} AS WL, ${tbProduct} AS P WHERE P.Id_product = WL.ID_SPYT AND WL.Id_Customer = ${id}`;
        const rows = await db.load(sql);
        return rows;
    },

   
};