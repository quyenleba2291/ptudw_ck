const db = require("../utils/db");

module.exports = {
  getEndProducts: async () => {
    const sql = `select * from Product WHERE Time_End_Submitted > now() ORDER BY Time_End_Submitted asc limit 5`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getHighestPriceProducts: async () => {
    const sql = `select * from Product ORDER BY Price desc LIMIT 5`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getTrendingProducts: async () => {
    const sql = `SELECT * from Product ORDER BY Number_Of_Current_Prices desc LIMIT 5`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },

  getInfoProducts: async id => {
    const sql = `SELECT * FROM Product WHERE Id_Product = ${id}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getInfoSellerOfProduct: async id => {
    const sql = `SELECT * FROM Customer WHERE Id_Customer = ${id}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },

  isLikeProduct: async (idUser, idPro) => {
    const sql = `SELECT * FROM Watch_List WHERE  Id_Customer = ${idUser} and ID_SPYT = ${idPro}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return true;
    }
    return false;
  },

  likeAProduct: async (idUser, idPro) => {
    let sql = `SELECT * FROM Watch_List WHERE  Id_Customer = ${idUser} and ID_SPYT = ${idPro}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      sql = `DELETE FROM Watch_List WHERE  Id_Customer = ${idUser} and ID_SPYT = ${idPro}`;
      await db.load(sql);
    } else {
      sql = `INSERT INTO Watch_List VALUES (${idUser}, ${idPro})`;
      await db.load(sql);
    }
    return null;
  },
  getProByCat: async idCat => {
    const sql = `SELECT * FROM Product WHERE Product.Id_Category = ${idCat}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
  },
  newSaleProduct: async newProduct => {
    let sql = `INERT INTO Product
         (${newProduct.Name},
            ${newProduct.MainImage},
            ${newProduct.SecondaryImage},
            ${newProduct.Category},
            ${newProduct.IdSeller},
            ${newProduct.Price},
            ${newProduct.PurchasePriceNow},
            ${newProduct.TimeSubmitted},
            ${newProduct.TimeEnd},
            ${newProduct.NumCurrentPrice},
            ${newProduct.NameTheHighestBidder},
            ${newProduct.PriceTheHighestBidder},
            ${newProduct.PriceStep},
            ${newProduct.NumberLike},
            ${newProduct.NumberDislike},
            ${newProduct.Evaluate},
            ${newProduct.Status}`;
    await db.load(sql);
    return null;
  },

  //Search support
  searchByType: async (query,searchType) =>{
    let sql = ``;
    if(searchType == "1"){
      sql = `SELECT * FROM Product Where Name_Product LIKE '%${query}%'`
    }
    else if (searchType == "2"){
      sql = `SELECT * FROM Product Where Id_Category = '${query}'`;
    }
    else if(searchType == "0"){
      sql = `SELECT * FROM Product Where Name_Product LIKE '%${query}%' OR Id_Category LIKE '%${query}%'`;
    }
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  searchByName: async query => {
    const sql = `SELECT * FROM Product Where Name_Product like '%${query}%'`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  searchByNameAndCat: async (query, catId) => {
    const sql = `SELECT * FROM Product Where Name_Product like '%${query}%' and Id_Category = ${catId}`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  searchByNameWithSort: async (query, mode) => {
    let sql = `SELECT * FROM Product Where Name_Product like '%${query}%'`;
    console.log("mode: ", mode);
    switch (mode) {
      case "asc":
        sql = `SELECT * FROM Product Where Name_Product like '%${query}%' ORDER BY Price ASC`;
        break;
      case "desc":
        sql = `SELECT * FROM Product Where Name_Product like '%${query}%'  ORDER BY Price DESC`;
        break;
      case "end":
        sql = `SELECT * FROM Product Where Name_Product like '%${query}%'  ORDER BY Time_End_Submitted ASC`;
        break;
    }
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getBidder: async id => {
    const sql = `SELECT * FROM History_Deal, Customer WHERE Id_Product = ${id} and Id_Buyer = Customer.Id_Customer ORDER BY Price desc LIMIT 5`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getAllBidder: async id => {
    const sql = `SELECT * FROM History_Deal, Customer WHERE Id_Product = ${id} and Id_Buyer = Customer.Id_Customer ORDER BY Price desc`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  },
  getBidderHighestPrice: async id => {
    const sql = `SELECT * FROM History_Deal, Customer WHERE Id_Product = ${id} and Id_Buyer = Customer.Id_Customer ORDER BY Price desc LIMIT 1`;
    const rs = await db.load(sql);
    if (rs.length > 0) {
      return rs;
    }
    return null;
  }

};
