var createError = require('http-errors');
var express = require('express');
var path = require('path');
var indexRouter = require('./controller/index');
var guestRouter = require('../../controller/index');

var app = express();
exphbs = require('express-handlebars');
/*config cho express handlebars*/
const hbs = exphbs.create({
    extname: 'hbs',
    layoutsDir:__dirname+'/views/layouts/',
    defaultLayout: 'layout',
    partialsDir:__dirname+'/views/partials/', 
    helpers: {
        add: function (A,B) { return A+B; },
       
    }   
})
app.engine('hbs', hbs.engine);
// view engine setup
// app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.get('/signout', guestRouter);
module.exports = app;