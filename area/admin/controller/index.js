var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home.hbs',{layout:"layout.hbs", title:"title admin"});
});
router.post('/', function(req, res, next){
  res.render('home.hbs',{layout:"layout.hbs", title: req.body.username});
});
module.exports = router;