 drop database Data_Web;
CREATE DATABASE Data_Web;
---------------------------------------------
USE Data_Web;
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;
---------------------------------------------

CREATE TABLE Kind_Of_Custom-- Thể loại khách hàng -> người bán người mua người xem
(
	Id_Kind_Of_Custom INT NOT NULL AUTO_INCREMENT,-- id thể loại khách hàng
	Name_Kind_Of_Custom VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- tên thể loại khách hàng
	CONSTRAINT PK_Kind_Of_Custom
    PRIMARY KEY(Id_Kind_Of_Custom)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-----------------------------------------------
create table Evaluate_Customer -- bảng đánh giá customer
(
	Id_Evaluate_Cus int auto_increment, 
    Id_Customer int, -- id người đc đánh giá
    Id_Man int, -- id người đánh giá
    N_Comment VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
    Dis_Like boolean, --  like or dislike
    CONSTRAINT PK_Evaluate_Customer
    PRIMARY KEY(Id_Evaluate_Cus)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
---------------------------------------------
create table Evaluate_Product -- bảng đánh giá sản phẩm
(
	Id_Evalute_Pro int auto_increment, 
    Id_Product int, -- id người đc đánh giá
    Id_Man int, -- id người đánh giá
    N_Comment VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
     Dis_Like boolean, -- like or dislike
    CONSTRAINT PK_Evaluate_Customer
    PRIMARY KEY(Id_Evalute_Pro)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
---------------------------------------------
CREATE TABLE Customer-- khách hàng
(
	Id_Customer INT ,-- id khách hàng
	Name_Customer VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- tên khách hàng
	-- ReCaptcha VARCHAR(3000),-- captcha xác nhận
	Address_Customer VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- địa chỉ
	Phone VARCHAR(80),-- sdt
	Id_Kind_Of_Custom int ,-- id thể loại khách hàng
	Information VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- thông tin
    N_Like_Bidder int, -- số lượt yêu thích
    N_Dislike_Bidder int, -- số lượt dis
     N_Like_Seller int, -- số lượt yêu thích
    N_Dislike_Seller int, -- số lượt dis
	Time_Active datetime, -- ngày còn lại seller
	Email VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- email
	CONSTRAINT PK_Customer
    PRIMARY KEY(Id_Customer)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
---------------------------------------------
CREATE TABLE CategoryLV1-- Thể loại
(
	Id_Category_Lv1 INT NOT NULL AUTO_INCREMENT,-- id loại sản phẩm
	Name_Category_Lv1 VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- tên loại sản phẩm
	CONSTRAINT PK_Category_lv1
    PRIMARY KEY(Id_Category_Lv1)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

---------------------------------------------------------
CREATE TABLE CategoryLV2-- Thể loại
(
	Id_Category_Lv2 INT NOT NULL AUTO_INCREMENT,-- id loại sản phẩm
	Name_Category_Lv2 VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- tên loại sản phẩm
    Id_Category_Lv1 int,
	CONSTRAINT PK_Category_lv2
    PRIMARY KEY(Id_Category_Lv2)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-------------------------------------------------------
---------------------------------------------------------
CREATE TABLE Promote-- thăng câp
(
	Id_Promote int AUTO_INCREMENT,
	Id_Customer INT ,-- id cus
	_Comment VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- lý do
    Invalid boolean,
	CONSTRAINT Promote
    PRIMARY KEY(Id_Promote)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-------------------------------------------------------

CREATE TABLE Product -- Sản phẩm
(
	Id_Product INT NOT NULL AUTO_INCREMENT,-- id sản phẩm
	Name_Product VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- tên sản phẩm
	Image_Product VARCHAR(3000),-- hình ảnh sản phẩm
	Image_Product_Ss1 VARCHAR(3000),-- hình ảnh phụ
	Image_Product_Ss2 VARCHAR(3000),-- hình ảnh phụ
    Image_Product_Ss3 VARCHAR(3000),-- hình ảnh phụ
	Id_Category int,-- id loại sản phẩm
    Id_Seller_Information int,-- id người bán
	Price int, -- giá bán ra
	Purchase_Price_Now int,-- giá mua ngay
	Time_Submitted_Product DATETIME,-- ngày đăng sản phẩm
	Time_End_Submitted datetime,-- thời gian còn lại
	Number_Of_Current_Prices INT,-- Số lượt ra giá hiện tại
	Name_The_Highest_Bidder_Product VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,-- thông tin người ra giá cao nhất
	Price_The_Highest_Bidder_product int,-- giá cao nhất
	Price_Step int ,-- bước giá
	N_Like int, -- số lượt yêu thích
    N_Dislike int, -- số lượt dis
	Evaluate_Product VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
    _Status boolean, -- trạng thái bán or đã bán
    Is_Not_Deleted boolean , -- 0 đã xóa 1 chưa xóa
	CONSTRAINT PK_Product
    PRIMARY KEY(Id_Product)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
----------------------------------------------------
CREATE TABLE Product_Buyer-- bảng người mua thành công
(
	Id_Deal int , -- id giao dịch thành công lấy từ id giao dịch history
	Id_Buyer INT  ,-- id người mua thành công
    Id_Product int , -- id sản phẩm mua thành công
	All_Price int, -- giá mua
	CONSTRAINT PK_Product_Buyer
    PRIMARY KEY(Id_Deal)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-----------------------------------------------------
CREATE TABLE Watch_List-- sản phẩm yêu thích
(
	-- Id_Watch int AUTO_INCREMENT,
	Id_Customer int , -- id người duungf
    ID_SPYT int , -- id sản phẩm yêu thích
	CONSTRAINT PK_watch_list
    PRIMARY KEY(Id_Customer,ID_SPYT)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-------------------------------------------------------
CREATE TABLE History_Deal-- lịch sử đấu giá của sản phẩm
(
	Id_History INT  AUTO_INCREMENT, -- id giao dịch
	Id_Seller int ,-- id người bán
    Id_Product int,-- id sản phẩm VARCHAR(30) NOT NULL,
	Id_Buyer int ,-- id người mua
    Invalid boolean , -- hợp lệ
	Date_Of_Sale DATETIME, -- ngày bán
	Price int, -- giá ddaasu	
    CONSTRAINT PK_History_Deal
    PRIMARY KEY(Id_History)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-----------------------------------------------------------------------

-------------------------------------------------------
CREATE TABLE User_Tab-- user
(
	Id int not null auto_increment ,-- id userr
    Email VARCHAR(3000),-- id roles
	Emailconfirmed bit,
	PasswordHash VARCHAR(3000),
	PhoneNumber VARCHAR(3000),
	UserName VARCHAR(3000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	CONSTRAINT PK_User
    PRIMARY KEY(Id)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


------------------------------------------------------------------------
CREATE TABLE N_Status-- bảng trạng thái người bán
(
	Id int , -- id
	Kind_Status VARCHAR(3000),  -- trạng thái 0 chưa bán 1 đã bán
	CONSTRAINT PK_N_Status
    PRIMARY KEY(Id)
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
---------------------------------------------------------------------
-- status
INSERT INTO `Data_Web`.`N_Status` (`Id`, `Kind_Status`) VALUES ('0', 'Chưa bán');
INSERT INTO `Data_Web`.`N_Status` (`Id`, `Kind_Status`) VALUES ('1', 'Đã bán');
----------------------------------------------------------------------------
-- kind of customer
INSERT INTO `Data_Web`.`Kind_Of_Custom` (`Id_Kind_Of_Custom`, `Name_Kind_Of_Custom`) VALUES ('1', 'Admin');
INSERT INTO `Data_Web`.`Kind_Of_Custom` (`Id_Kind_Of_Custom`, `Name_Kind_Of_Custom`) VALUES ('2', 'Bidder');
INSERT INTO `Data_Web`.`Kind_Of_Custom` (`Id_Kind_Of_Custom`, `Name_Kind_Of_Custom`) VALUES ('3', 'Seller');
----------------------------------------------------------------------------
-- user
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('1', 'nguyenthanh@gmail.com', 1, '133252ffs', '0385678756', 'nguyenthanh');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('2', 'thanhkhuong@gmail.com', 1, 'bnyixkd32', '0347694233', 'thanhkhuong');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('3', 'hongphuc@gmail.com', 1, 'qffsasfaaa3', '0347694232', 'hongphuc');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('4', 'thanhlong@gmail.com', 1, 'ffeefeffff233', '0347695252', 'thanhlong');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('5', 'thangnguyen@gmail.com', 1, 'thangnguyen25', '0385297599', 'thangnguyen');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('6', 'quyenle@gmail.com', 1, 'quyen1998', '0386883753', 'quyenle');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('7', 'thangvu@gmail.com', 1, 'thangvu1999', '0389235252', 'thangvu');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('8','duyquang@gmail.com', 1, 'duyquang2k', '0347123235', 'duyquang');
INSERT INTO `Data_Web`.`User_Tab` (`Id`, `Email`, `Emailconfirmed`, `PasswordHash`, `PhoneNumber`, `UserName`) VALUES ('9','mrngunuong1@gmail.com', 1, 'b007dbc7e57b3e1654ce5f94b6fdd9a2c88978233a4408de45a9e9ba14415d0916f58af1880', '0707488948', 'vpdthang');
--------------------------------------------------------------------------------------------------------------------------
-- Customer
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('1', 'Hoàng văn Phước', 'Le Nin Street,Nghe An', '0385678756', '1', 'Mental health counselor', '32', '0', '2019-11-22 11:15:33', 'nguyenthanh@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('2', 'Hoàng Nghĩa Tình', '41 Tran Hung Dao Street Kon Tum Township,  KonTum', '0347694233', '2', 'Broker associate', '25', '0', '2019-11-22 19:11:33', 'thanhkhuong@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('3', 'Nguyễn Văn Sử', '374/25 Le Hong Phong Street, Ward 1, District 10,Ho Chi Minh City', '0347694232', '2', 'Gaming change person', '32', '5', '2019-11-20 14:11:33', 'hongphuc@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('4', 'Đinh Quốc Phương', 'Dai Co Viet St., 8C Building, Bach Khoa Ward,Hanoi', '0347695252', '3', 'Medical technician', '42', '1', '2019-11-22 21:11:33', 'thanhlong@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('5', 'Nguyễn hoàng Trung', '22/11 Nguyen Hien, Dist.3,Ho Chi Minh City', '0385297599', '2', 'Psychiatric nursing assistant', '41', '4', '2019-11-20 19:11:33', 'thangnguyen@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('6', 'Bùi Đăng Long', '19 Ngo Quyen St., Ward 7,Ho Chi Minh City', '0386883753', '3', 'University faculty', '4', '4', '2019-11-23 19:11:33', 'quyenle@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('7', 'Đặng Hiền', '136/36 Nguyen Thi Tan Street, Ward 2, District 8,Ho Chi Minh City', '0389235252', '2', 'Science writer', '1', '0', '2019-11-19 20:23:33', 'thangvu@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('8', 'Nguyễn Hữu Thắng', '98 Nguyen Dinh Chieu, Dist1,Ho Chi Minh City', '0347123235', '2', 'Aircraft structure assembler', '0', '0', '2019-11-19 17:11:33', 'duyquang@gmail.com');
INSERT INTO `Data_Web`.`Customer` (`Id_Customer`, `Name_Customer`, `Address_Customer`, `Phone`, `Id_Kind_Of_Custom`, `Information`, `N_Like_Bidder`, `N_Dislike_Bidder`, `Time_Active`, `Email`) VALUES ('9', 'Đức Thắng', '69 tổ 4 khu phố 9', '0707488948', '2', 'Aircraft structure assembler', '0', '0', '2019-11-19 21:11:49', 'mrngunuong1@gmail.com');
UPDATE `data_web`.`customer` SET `Id_Kind_Of_Custom` = '3' WHERE (`Id_Customer` = '9');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '1');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '2');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '3');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '20', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '4');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '5');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '10', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '6');
UPDATE `data_web`.`customer` SET `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '7');
UPDATE `data_web`.`customer` SET `N_Like_Bidder` = '4', `N_Like_Seller` = '0', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '8');
UPDATE `data_web`.`customer` SET `N_Like_Bidder` = '6', `N_Like_Seller` = '10', `N_Dislike_Seller` = '0' WHERE (`Id_Customer` = '9');
UPDATE `data_web`.`customer` SET `Id_Kind_Of_Custom` = '2' WHERE (`Id_Customer` = '1');

---------------------------------------------------------------------------------------------------------------------------
--  Product
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_Product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('1', 'Samsung Galaxy J7 Prime', '', '', '1', '4', '7000000', '5100000', '2019-11-23 19:11:33', '2020-01-5 19:12:33', '5', 'Hoàng Văn Long', '6100000', '100000', '10', '0', 'Galaxy J7 Prime xứng đáng là cái tên hàng đầu trong danh sách chọn smartphone phổ thông của giới trẻ khi nhận được nhiều đánh giá tích cực về thiết kế, thời lượng pin lâu dài và camera chụp hình chất lượng tốt.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('2', 'Asus Zenfone 4 Max Pro', '', '', '1', '4', '6100000', '1000000', '2019-11-23 19:11:33', '2020-01-06 19:11:33', '5', '100000', '12', '0', 'Asus ZenFone 4 Max Pro là sản phẩm có mức giá rẻ nhất được Asus giới thiệu trong sự kiện vào tháng 8 vừa rồi, ưu điểm lớn nhất của máy chính là viên pin dung lượng khủng cùng bộ đôi camera kép ấn tượng.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_Product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('3', 'OPPO F7 Đen', '', '', '1', '4', '4900000', '4100000', '2019-11-23 19:11:33', '2020-01-07 18:11:33', '6', 'Hoàng Nghĩa Tình', '4900000', '100000', '34', '0', 'Thiết kế tương tự như những dòng OPPO F7 phiên bản thường, nhưng mặt lưng của máy được làm vân kim cương rất sang trọng và \"ngầu\". Mặt lưng dạng này chúng ta đã từng thấy trên sản phẩm Mirror 5 của hãng nhưng lần này với tông màu tối hơn cho cảm giác sang trọng.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('4', 'OPPO A37 (Neo 9)', '', '', '1', '4', '1000000', '4100000', '2019-11-23 19:11:33', '2020-01-05 20:11:33', '8', '100000', '24', '0', 'OPPO A37 (Neo 9) là thiết bị tiếp theo của dòng OPPO Neo vốn được người dùng yêu thích với camera selfie ảo diệu cùng mức giá bán phải chăng.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('5', 'Xiaomi Redmi Note 4', '', '', '1', '4', '7000000', '6700000', '2019-11-23 19:11:33', '2020-01-07 19:11:33', '5', '100000', '1', '0', 'Đặc điểm nổi bật của Xiaomi Redmi Note 4', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('6', 'Nokia 6.1 (2018)', '', '', '1', '4', '7000000', '66000000', '2019-11-23 19:11:33', '2020-01-10 19:15:33', '4', '100000', '5', '0', 'Sau nhiều rò rỉ thì cuối cùng chiếc Nokia 6.1 (Nokia 6 new) cũng đã chính thức ra mắt với một thiết kế sang trọng nhưng vẫn có gì đó đáng tiếc cho một chiếc smartphone ra mắt vào năm 2018.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('7', 'Huawei MateBook 13', '', '', '2', '6', '23000000', '2100000', '2019-11-23 19:11:33', '2020-01-12 12:11:33', '4', '100000', '2', '0', 'Không chịu “an phận” trong thị trường smartphone, Huawei đang ngày càng“lấn sân” sang ngành công nghiệp laptop và mới đầu năm nay, họ đã ra mắt chiếc Huawei MateBook 13, bản nâng cấp của Huawei MateBook Pro X vào năm ngoái. ', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('8', 'Dell XPS 13', '', '', '2', '6', '2100000', '20000000', '2019-11-23 19:11:33', '2020-01-11 17:16:33', '4', '100000', '52', '0', 'Tại CES 2019, Dell đã trình làng chiếc Dell XPS 13 và được đánh giá rất cao với nhiều cải tiến so với phiên bản tiền nhiệm. ', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('9', 'ASUS ZenBook Flip S UX370', '', '', '2', '6', '22000000', '2100000', '2020-01-03 19:11:33', '2019-12-01 15:11:33', '2', '100000', '6', '0', 'ASUS đã làm mới mới dòng máy tính xách tay ZenBook Flip S 2 trong 1 của mình với ASUS ZenBook Flip S UX370, gây ấn tượng khá mạnh và thẳng tiến ở vị trí thứ 3 trong danh sách những chiếc laptop tốt nhất 2019 do TechRadar bình chọn.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('10', 'Apple MacBook Pro 13 inch 2018', '', '', '2', '6', '23000000', '22000000', '2020-01-08 19:11:33', '2019-12-02 21:11:33', '8', '100000', '3', '0', 'Apple luôn chứng tỏ mình là một đối thủ đáng gờm trong ngành công nghiệp máy tính. Năm ngoái, hãng đã ra mắt chiếc Macbook Pro Touch Bar 13 inch, được đánh giá là chiếc MacBook tốt nhất từ ​​trước đến nay.', '0');
INSERT INTO `Data_Web`.`Product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`) VALUES ('11', 'MSI GS65 Stealth', '', '', '2', '6', '21000000', '23000000', '2019-11-23 19:11:33', '2020-01-05 22:12:33', '7', '100000', '6', '0', 'MSI GS65 Stealth là chiếc laptop chơi game duy nhất xuất hiện trong danh sách 5 chiếc laptop tốt nhất 2019 của TechRadar. Thiết bị là một trong những chiếc laptop gaming mỏng nhất khi chỉ dày 1.79 mm, màn hình 15.6 inch, độ phân giải full HD cùng tốc độ làm tươi 144 Hz. ', '0');--

UPDATE `Data_Web`.`Product` SET `Image_Product` = 'Images/1/1.jpg' WHERE (`Id_Product` = '1');
UPDATE `data_web`.`product` SET `Image_Product_Ss1` = 'Images/1/2.jpg', `Image_Product_Ss2` = 'Images/1/3.jpg', `Image_Product_Ss3` = 'Images/1/4.jpg' WHERE (`Id_Product` = '1');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/2/1.jpg', `Image_Product_Ss1` = 'Images/2/2.jpg', `Image_Product_Ss2` = 'Images/2/3.jpg', `Image_Product_Ss3` = 'Images/2/4.jpg' WHERE (`Id_Product` = '2');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/3/1.jpg', `Image_Product_Ss1` = 'Images/3/2.jpg', `Image_Product_Ss2` = 'Images/3/3.jpg', `Image_Product_Ss3` = 'Images/3/4.jpg' WHERE (`Id_Product` = '3');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/4/1.jpg', `Image_Product_Ss1` = 'Images/4/2.jpg', `Image_Product_Ss2` = 'Images/4/3.jpg', `Image_Product_Ss3` = 'Images/4/4.jpg' WHERE (`Id_Product` = '4');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/5/1.jpg', `Image_Product_Ss1` = 'Images/5/2.jpg', `Image_Product_Ss2` = 'Images/5/3.jpg', `Image_Product_Ss3` = 'Images/5/4.jpg' WHERE (`Id_Product` = '5');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/6/1.jpg', `Image_Product_Ss1` = 'Images/6/2.jpg', `Image_Product_Ss2` = 'Images/6/3.jpg', `Image_Product_Ss3` = 'Images/6/4.jpg' WHERE (`Id_Product` = '6');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/7/1.jpg', `Image_Product_Ss1` = 'Images/7/2.jpg', `Image_Product_Ss2` = 'Images/7/3.jpg', `Image_Product_Ss3` = 'Images/7/4.jpg' WHERE (`Id_Product` = '7');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/8/1.jpg', `Image_Product_Ss1` = 'Images/8/2.jpg', `Image_Product_Ss2` = 'Images/8/3.jpg', `Image_Product_Ss3` = 'Images/8/4.jpg' WHERE (`Id_Product` = '8');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/9/1.jpg', `Image_Product_Ss1` = 'Images/9/2.jpg', `Image_Product_Ss2` = 'Images/9/3.jpg', `Image_Product_Ss3` = 'Images/9/4.jpg' WHERE (`Id_Product` = '9');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/10/1.jpg', `Image_Product_Ss1` = 'Images/10/2.jpg', `Image_Product_Ss2` = 'Images/10/3.jpg', `Image_Product_Ss3` = 'Images/10/4.jpg' WHERE (`Id_Product` = '10');
UPDATE `data_web`.`product` SET `Image_Product` = 'Images/11/1.jpg', `Image_Product_Ss1` = 'Images/11/2.jpg', `Image_Product_Ss2` = 'Images/11/3.jpg', `Image_Product_Ss3` = 'Images/11/4.jpg' WHERE (`Id_Product` = '11');


UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '1');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '2');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '3');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '4');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '5');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '6');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '7');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '8');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '9');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '10');
UPDATE `Data_Web`.`Product` SET `Price_Step` = '10000' WHERE (`Id_Product` = '11');


UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '4', `Price_Step` = '10000' WHERE (`Id_Product` = '1');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '2', `Price_Step` = '10000' WHERE (`Id_Product` = '2');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '2', `Price_Step` = '10000' WHERE (`Id_Product` = '3');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '1', `Price_Step` = '10000' WHERE (`Id_Product` = '4');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '1', `Price_Step` = '10000' WHERE (`Id_Product` = '5');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '0', `Price_Step` = '10000' WHERE (`Id_Product` = '6');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '0', `Price_Step` = '10000' WHERE (`Id_Product` = '7');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '1', `Price_Step` = '10000' WHERE (`Id_Product` = '8');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '5', `Price_Step` = '10000' WHERE (`Id_Product` = '9');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '3', `Price_Step` = '10000' WHERE (`Id_Product` = '10');
UPDATE `Data_Web`.`Product` SET `Number_Of_Current_Prices` = '1', `Price_Step` = '10000' WHERE (`Id_Product` = '11');

UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Hoàng Nghĩa Tình', `Price_The_Highest_Bidder_Product` = '5900000' WHERE (`Id_Product` = '2');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Đặng Hiền', `Price_The_Highest_Bidder_Product` = '4600000' WHERE (`Id_Product` = '4');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Nguyễn Văn Sử', `Price_The_Highest_Bidder_Product` = '6800000' WHERE (`Id_Product` = '5');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Nguyễn Văn Sử', `Price_The_Highest_Bidder_Product` = '20100000' WHERE (`Id_Product` = '8');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Hoàng Nghĩa Tình', `Price_The_Highest_Bidder_Product` = '21900000' WHERE (`Id_Product` = '9');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Nguyễn hoàng Trung', `Price_The_Highest_Bidder_Product` = '22900000' WHERE (`Id_Product` = '10');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Đặng Hiền', `Price_The_Highest_Bidder_Product` = '24000000' WHERE (`Id_Product` = '11');
UPDATE `Data_Web`.`Product` SET `_Status` = '1' WHERE (`Id_Product` = '1');
UPDATE `Data_Web`.`Product` SET `_Status` = '1' WHERE (`Id_Product` = '3');


UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '14000000' WHERE (`Id_Product` = '1');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '12000000' WHERE (`Id_Product` = '2');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '9900000' WHERE (`Id_Product` = '3');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '9000000' WHERE (`Id_Product` = '4');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '11700000' WHERE (`Id_Product` = '5');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '12000000' WHERE (`Id_Product` = '6');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '28000000' WHERE (`Id_Product` = '7');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '27000000' WHERE (`Id_Product` = '8');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '29100000' WHERE (`Id_Product` = '9');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '3100000' WHERE (`Id_Product` = '10');
UPDATE `Data_Web`.`Product` SET `Purchase_Price_Now` = '33000000' WHERE (`Id_Product` = '11');


UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Nguyễn Hữu Thắng', `Price_The_Highest_Bidder_Product` = '14000000' WHERE (`Id_Product` = '1');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '710000' WHERE (`Id_Product` = '2');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '9000000' WHERE (`Id_Product` = '3');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '8600000' WHERE (`Id_Product` = '4');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '7800000' WHERE (`Id_Product` = '5');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '24100000' WHERE (`Id_Product` = '8');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Đinh Quốc Phương', `Price_The_Highest_Bidder_Product` = '2610000' WHERE (`Id_Product` = '9');
UPDATE `Data_Web`.`Product` SET `Name_The_Highest_Bidder_Product` = 'Hoàng Nghĩa Tình', `Price_The_Highest_Bidder_Product` = '27000000' WHERE (`Id_Product` = '10');
UPDATE `Data_Web`.`Product` SET `Price_The_Highest_Bidder_Product` = '30000000' WHERE (`Id_Product` = '11');


UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '1');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '2');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '3');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '4');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '5');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '6');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '7');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '8');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '9');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '10');
UPDATE `data_web`.`product` SET `Is_Not_Deleted` = '1' WHERE (`Id_Product` = '11');

INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('12', 'Điện thoại Vsmart Live (64GB/6GB) - Hàng chính hãng', 'Images/12/1.jpg', 'Images/12/2.jpg', 'Images/12/3.jpg', 'Images/12/4.jpg', '1', '4', '7790000', '12000000', '2019-11-27 19:11:33', '2020-01-08 20:14:33', '1', 'Đức Thắng', '10000000', '10000', '5', '0', 'Điện Thoại Vsmart Live với 3 camera đẳng cấp, cảm biến vân tay dưới màn hình cùng hiệu năng xuất sắc, Vsmart Live mang đến cho bạn cuộc sống phong cách và đầy bứt phá.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('13', 'Điện Thoại iPhone 11 Pro Max 64GB', 'Images/13/1.jpg', 'Images/13/2.jpg', 'Images/13/3.jpg', 'Images/13/4.jpg', '1', '6', '30490000', '35000000', '2019-11-27 00:01:33', '2020-01-08 20:33:33', '2', 'Đinh Quốc Phương', '33000000', '10000', '10', '0', 'Điện thoại iPhone 11 Pro Max là phiên bản cao cấp nhất của iPhone năm nay. Sản phẩm có nhiều cải tiến nổi bật, hiệu năng, thiết kế mới đặc biệt ở phần mặt lưng và hệ thống camera.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('14', 'Điện Thoại iPhone 7 Plus - Hàng Chính Hãng VN/A', 'Images/14/1.jpg', 'Images/14/2.jpg', 'Images/14/3.jpg', 'Images/14/4.jpg', '1', '9', '11290000', '14000000', '2019-11-24 00:41:33', '2020-01-08 22:43:33', '1', 'Đặng Hiền', '13500000', '10000', '12', '0', 'Điện Thoại iPhone 7 Plus với kích thước 158.2 x 77.9 x 7.3 mm mỏng nhẹ và thiết kế tương tự như bộ đôi iPhone 6s/6s Plus, tuy nhiên phần dải nhựa bắt sóng không cắt ngang mặt lưng như phiên bản cũ mà được chuyển sang phần cạnh trên của sản phẩm. Phím Home vật lý trên điện thoại cũng được thay thế bằng phím cảm ứng nhờ vào sự kết hợp Taptic Engine mới và liên kết với 3D Touch tiện lợi và đẹp mắt.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('15', 'iPad Air 10.5 Wi-Fi 64GB New 2019', 'Images/15/1.jpg', 'Images/15/2.jpg', 'Images/15/3.jpg', 'Images/15/4.jpg', '2', '9', '12490000', '15500000', '2019-11-27 07:01:33', '2020-01-11 20:23:33', '1', 'Hoàng Nghĩa Tình', '15000000', '10000', '10', '0', 'iPad Air 10.5 Wi-Fi 64GB New 2019 ở hữu thiết kế sang trọng với phần viền màn hình mỏng hơn so với các thế hệ trước kết hợp kích thước 10.5 inch, tỷ lệ khung hình 4:3 mang lại cho người dùng không gian trải nghiệm rộng rãi.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('16', 'Apple Macbook Air 2019', 'Images/16/1.jpg', 'Images/16/2.jpg', 'Images/16/3.jpg', 'Images/16/4.jpg', '2', '9', '24899000', '29000000', '2019-11-27 17:01:33', '2020-01-11 20:23:33', '1', 'Nguyễn Văn Sử', '26000000', '10000', '9', '1', 'Apple Macbook Air 2019 - 13 inchs (i5/ 8GB/ 128GB) sở hữu màn hình LED-backlit 13.3 inch (2560 x 1600) kết hợp với công nghệ Retina True Tone giúp bạn xem mọi thứ tốt hơn, mang đến những hình ảnh chất lượng, chi tiết, rõ nét, màu sắc cũng cực kỳ trung thực và sống động.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('17', 'Laptop Asus Zenbook UX433FA-A6061T Core i5-8265U/Win10 (14\" FHD IPS) ', 'Images/17/1.jpg', 'Images/17/2.jpg', 'Images/17/3.jpg', 'Images/17/4.jpg', '2', '9', '20290000', '25000000', '2019-11-27 23:01:33', '2020-01-11 04:23:33', '1', 'Đinh Quốc Phương', '22000000', '10000', '4', '0', 'Asus Zenbook UX433FA i5 (A6061T) được hoàn thiện tỉ mỉ từ kim loại nguyên khối với độ dày siêu mỏng chỉ 15.9mm mang đến cho sản phẩm một vẻ đẹp cực kì sang trọng và hiện đại.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('18', 'Laptop Acer Predator Helios 300 PH315-52-78HH NH.Q53SV.008 Core i7-9750H/ GTX 1660Ti 6GB/ Win10 (15.6 FHD IPS 144Hz) ', 'Images/18/1.jpg', 'Images/18/2.jpg', 'Images/18/3.jpg', 'Images/18/4.jpg', '2', '9', '35339000', '42000000', '2019-11-27 22:01:33', '2020-01-11 02:23:33', '1', 'Hoàng văn Phước', '39000000', '10000', '7', '0', 'aptop Acer Predator Helios 300 PH315-52-78HH NH.Q53SV.008 đã có nâng cấp vượt trội với thiết kế bên ngoài mang đẳng cấp vượt thời gian. Acer đã trang bị cho chiếc laptop gaming này vỏ kim loại ở mặt trên và phần chiếu nghỉ tay cực kỳ chắc chắn, tô điểm bằng những đường nét màu xanh electric giống như những máy thuộc thương hiệu Predator khác, so với tông màu đỏ như thế hệ trước. Điều này giúp Acer khẳng định lại thương hiệu Predator ở thiết kế bên ngoài, mang lại ngoại hình mới cho dòng laptop chiến game trên các cỗ máy cao cấp.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('19', 'Laptop Asus ROG Strix G G531-VAL052T Core i7-9750H/ RTX 2060 6GB/ Win10', 'Images/19/1.jpg', 'Images/19/2.jpg', 'Images/19/3.jpg', 'Images/19/4.jpg', '2', '9', '36749000', '44000000', '2019-11-27 07:01:33', '2020-01-10 05:23:33', '2', 'Nguyễn Hữu Thắng', '42000000', '10000', '14', '0', 'Laptop Asus ROG Strix G G531-VAL052T Core i7-9750H/ RTX 2060 6GB/ Win10 (15.6 FHD NanoEdge IPS 120Hz) là hiện thân của phong cách thiết kế tối giản, mang đến trải nghiệm cốt lõi mãnh liệt nhất, dư sức để chơi những tựa game nặng và xử lý đa nhiệm trên Windows 10 Home. Trang bị bộ vi xử lý Intel Core thế hệ thứ 9 mới nhất và đồ họa GeForce RTX 2060, chiếc laptop gaming này mang hiệu năng chơi game đầy thuyết phục tới đông đảo giới game thủ.', '0', '1');
INSERT INTO `data_web`.`product` (`Id_Product`, `Name_Product`, `Image_Product`, `Image_Product_Ss1`, `Image_Product_Ss2`, `Image_Product_Ss3`, `Id_Category`, `Id_Seller_Information`, `Price`, `Purchase_Price_Now`, `Time_Submitted_Product`, `Time_End_Submitted`, `Number_Of_Current_Prices`, `Name_The_Highest_Bidder_Product`, `Price_The_Highest_Bidder_product`, `Price_Step`, `N_Like`, `N_Dislike`, `Evaluate_Product`, `_Status`, `Is_Not_Deleted`) VALUES ('20', 'Điện Thoại iPhone XS Max 64GB', 'Images/20/1.jpg', 'Images/20/2.jpg', 'Images/20/3.jpg', 'Images/20/4.jpg', '1', '4', '25990000', '30000000', '2019-11-27 00:01:33', '2020-01-09 20:23:33', '2', 'Đức Thắng', '27000000', '10000', '21', '1', 'Điện Thoại iPhone XS Max 64GB - Nhập Khẩu Chính Hãng sở hữu màn hình OLED 6.5\". Đây là chiếc iPhone có màn hình lớn nhất từ trước đến nay. Mật độ điểm ảnh vẫn được giữ ở mức 458 ppi nhưng màn hình lớn hơn nên XS Max có độ phân giải 3.688 x 1.242 pixel với 3.3 triệu điểm ảnh.', '0', '1');

UPDATE `data_web`.`product` SET `Time_Submitted_Product` = '2019-12-03 19:11:33', `Time_End_Submitted` = '2020-01-08 15:11:33' WHERE (`Id_Product` = '9');
UPDATE `data_web`.`product` SET `Time_Submitted_Product` = '2019-12-01 19:11:33', `Time_End_Submitted` = '2020-01-09 21:11:33' WHERE (`Id_Product` = '10');


--------------------------------------------------------------------------------------------------------------------------------------
-- Product - buyer
INSERT INTO `Data_Web`.`Product_Buyer` (`Id_Deal`, `Id_Buyer`, `Id_Product`, `All_Price`) VALUES ('1', '8', '1', '14000000');
INSERT INTO `Data_Web`.`Product_Buyer` (`Id_Deal`, `Id_Buyer`, `Id_Product`, `All_Price`) VALUES ('2', '2', '3', '9000000');


-----------------------------------------
-- history
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('1', '4', '1', '8', '1', '2019-11-27 12:11:33', '6100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('2', '4', '3', '2', '1', '2019-11-26 9:11:33', '4900000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('3', '4', '1', '1', '0', '2019-11-27 9:11:33', '5100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('4', '4', '2', '2', '0', '2019-11-26 8:11:33', '5900000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('5', '4', '3', '3', '0', '2019-11-23 9:12:33', '4800000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('6', '4', '4', '7', '0', '2019-11-26 9:11:33', '4600000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('7', '4', '5', '3', '0', '2019-11-26 9:11:33', '6800000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('8', '4', '1', '8', '0', '2019-11-26 9:11:33', '5600000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('9', '4', '2', '2', '0', '2019-11-26 9:11:33', '510000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('10', '4', '1', '5', '0', '2019-11-26 23:11:33', '610000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('11', '6', '8', '3', '0', '2019-11-26 18:11:33', '20100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('12', '6', '9', '3', '0', '2019-11-26 9:11:33', '21100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('13', '6', '10', '3', '0', '2019-11-26 3:11:33', '22100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('14', '6', '9', '4', '0', '2019-11-26 19:11:33', '2110000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('15', '6', '9', '1', '0', '2019-11-26 9:11:33', '21600000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('16', '6', '9', '2', '0', '2019-11-26 9:11:33', '21900000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('17', '6', '10', '5', '0', '2019-11-26 21:11:33', '22900000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('18', '6', '11', '7', '0', '2019-11-26 9:11:33', '24000000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('19', '6', '9', '3', '0', '2019-11-26 4:11:33', '2100000');
INSERT INTO `Data_Web`.`History_Deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('20', '6', '10', '2', '0', '2019-11-26 9:11:33', '22000000');

UPDATE `Data_Web`.`History_Deal` SET `Price` = '14000000' WHERE (`Id_History` = '1');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '7200000' WHERE (`Id_History` = '3');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '7300000' WHERE (`Id_History` = '8');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '9800000' WHERE (`Id_History` = '10');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '6900000' WHERE (`Id_History` = '4');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '710000' WHERE (`Id_History` = '9');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '9000000' WHERE (`Id_History` = '2');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '8800000' WHERE (`Id_History` = '5');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '8600000' WHERE (`Id_History` = '6');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '7800000' WHERE (`Id_History` = '7');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '24100000' WHERE (`Id_History` = '11');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '25100000' WHERE (`Id_History` = '12');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '2610000' WHERE (`Id_History` = '14');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '25800000' WHERE (`Id_History` = '15');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '24900000' WHERE (`Id_History` = '16');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '25900000' WHERE (`Id_History` = '19');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '24100000' WHERE (`Id_History` = '13');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '24900000' WHERE (`Id_History` = '17');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '27000000' WHERE (`Id_History` = '20');
UPDATE `Data_Web`.`History_Deal` SET `Price` = '30000000' WHERE (`Id_History` = '18');

INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('21', '4', '12', '9', '0', '2019-12-27 09:11:33', '10000000');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 10:11:33' WHERE (`Id_History` = '12');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 03:11:33' WHERE (`Id_History` = '13');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 19:11:33', `Price` = '26100000' WHERE (`Id_History` = '14');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 11:11:33' WHERE (`Id_History` = '15');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 09:11:33' WHERE (`Id_History` = '16');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 21:11:33' WHERE (`Id_History` = '17');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-28 09:11:33' WHERE (`Id_History` = '18');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 14:11:33' WHERE (`Id_History` = '19');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-27 09:11:33' WHERE (`Id_History` = '20');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('22', '6', '13', '9', '0', '2019-12-28 09:10:33', '32000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('23', '6', '13', '4', '0', '2019-12-29 09:11:33', '33000000');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-29 12:11:33' WHERE (`Id_History` = '1');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-22 09:11:33' WHERE (`Id_History` = '3');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-23 09:11:33' WHERE (`Id_History` = '8');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-27 23:11:33' WHERE (`Id_History` = '10');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('24', '9', '14', '7', '0', '2019-12-29 09:11:33', '13500000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('25', '9', '15', '2', '0', '2019-12-29 09:11:33', '15000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('26', '9', '16', '3', '0', '2019-12-29 09:11:33', '26000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('27', '9', '17', '4', '0', '2019-12-29 09:11:33', '22000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('28', '9', '18', '1', '0', '2019-12-29 09:11:33', '39000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('29', '9', '19', '8', '0', '2019-12-29 09:11:33', '42000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('30', '9', '19', '3', '0', '2019-12-28 09:11:33', '41000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('31', '4', '20', '9', '0', '2019-12-29 19:11:33', '27000000');
INSERT INTO `data_web`.`history_deal` (`Id_History`, `Id_Seller`, `Id_Product`, `Id_Buyer`, `Invalid`, `Date_Of_Sale`, `Price`) VALUES ('32', '4', '20', '6', '0', '2019-12-29 07:11:33', '26000000');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 08:11:33' WHERE (`Id_History` = '4');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 09:11:33' WHERE (`Id_History` = '9');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 09:11:33' WHERE (`Id_History` = '2');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-23 09:12:33' WHERE (`Id_History` = '5');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 09:11:33' WHERE (`Id_History` = '6');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 09:11:33' WHERE (`Id_History` = '7');
UPDATE `data_web`.`history_deal` SET `Date_Of_Sale` = '2019-12-26 18:11:33' WHERE (`Id_History` = '11');

-- SELECT * FROM Data_Web.History_Deal;
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Promote
INSERT INTO `Data_Web`.`Promote` (`Id_Promote`, `Id_Customer`, `_Comment`, `Invalid`) VALUES ('1', '3', 'xin phép được bán sản phẩm', '0');
INSERT INTO `Data_Web`.`Promote` (`Id_Promote`, `Id_Customer`, `_Comment`, `Invalid`) VALUES ('2', '2', 'xin phép được bán sản phẩm', '0');
INSERT INTO `Data_Web`.`Promote` (`Id_Promote`, `Id_Customer`, `_Comment`, `Invalid`) VALUES ('3', '8', 'xin phép được bán sản phẩm', '0');
------------------------------------------------------------------------
-- watchlist
INSERT INTO `data_web`.`watch_list` (`Id_Customer`, `ID_SPYT`) VALUES ('1', '2');
INSERT INTO `data_web`.`watch_list` (`Id_Customer`, `ID_SPYT`) VALUES ('3', '6');
INSERT INTO `data_web`.`watch_list` (`Id_Customer`, `ID_SPYT`) VALUES ('2', '3');
INSERT INTO `data_web`.`watch_list` (`Id_Customer`, `ID_SPYT`) VALUES ('1', '5');
INSERT INTO `data_web`.`watch_list` (`Id_Customer`, `ID_SPYT`) VALUES ('1', '6');

------------------------------------------------------------------------------------
-- evaluate cus
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('1', '2', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('2', '3', '1', 'bad', '0');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('3', '4', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('4', '7', '5', 'bad', '0');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('5', '3', '8', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('6', '2', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('7', '5', '7', 'bad', '0');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('8', '2', '1', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('9', '5', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('10', '6', '8', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('11', '1', '4', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('12', '4', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('13', '6', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('14', '1', '5', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('15', '3', '6', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('16', '6', '5', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('17', '8', '6', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('18', '2', '1', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('19', '4', '3', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('20', '5', '2', 'good', '1');
INSERT INTO `data_web`.`evaluate_customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('21', '9', '1', 'good', '1');
INSERT INTO `data_web`.`evaluate_customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('22', '9', '3', 'ok', '1');
INSERT INTO `data_web`.`evaluate_customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('23', '9', '6', 'ok', '1');
INSERT INTO `data_web`.`evaluate_customer` (`Id_Evaluate_Cus`, `Id_Customer`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('24', '9', '4', 'good', '1');

--------------------------------------------------------------------------------------
-- elavuce pro
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('1', '1', '2', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('2', '2', '1', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('3', '3', '2', 'bad', '0');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('4', '4', '7', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('5', '5', '3', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('6', '6', '8', 'good', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('7', '7', '3', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('8', '3', '3', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('9', '7', '4', 'sản phẩm tốt', '1');
INSERT INTO `Data_Web`.`Evaluate_Product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('10', '9', '2', 'sản phẩm tốt', '1');

INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('11', '10', '8', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('12', '11', '8', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('13', '12', '8', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('14', '13', '1', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('15', '14', '1', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('16', '15', '1', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('17', '16', '2', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('18', '17', '2', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('19', '18', '2', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('20', '19', '3', 'sản phẩm tốt', '1');
INSERT INTO `data_web`.`evaluate_product` (`Id_Evalute_Pro`, `Id_Product`, `Id_Man`, `N_Comment`, `Dis_Like`) VALUES ('21', '20', '3', 'sản phẩm tốt', '1');

---------------------------------------------------------------------------------------------
-- category lv 1
INSERT INTO `Data_Web`.`Categorylv1` (`Id_Category_Lv1`, `Name_Category_Lv1`) VALUES ('1', 'Thiết bị điện tử');
INSERT INTO `Data_Web`.`Categorylv1` (`Id_Category_Lv1`, `Name_Category_Lv1`) VALUES ('2', 'Đồ gia dụng');

------------------------------------------------
-- category lv 2
INSERT INTO `Data_Web`.`Categorylv2` (`Id_Category_Lv2`, `Name_Category_Lv2`, `Id_Category_Lv1`) VALUES ('1', 'Điện thoại', '1');
INSERT INTO `Data_Web`.`Categorylv2` (`Id_Category_Lv2`, `Name_Category_Lv2`, `Id_Category_Lv1`) VALUES ('2', 'Laptop', '1');
----------------------------------------------------------------------------------------------------------------------------------
UPDATE `data_web`.`user_tab` SET `PasswordHash` = 'b007dbc7e57b3e1654ce5f94b6fdd9a2c88978233a4408de45a9e9ba14415d0916f58af1880' WHERE (`Id` = '1');
UPDATE `data_web`.`user_tab` SET `PasswordHash` = 'b007dbc7e57b3e1654ce5f94b6fdd9a2c88978233a4408de45a9e9ba14415d0916f58af1880' WHERE (`Id` = '6');
