const mysql = require('mysql');

function createConnection() {

    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        //password: '1234',
        database: 'Data_Web',
        typeCast: function castField(field, useDefaultTypeCasting) {
            if (field.type === "BIT" && field.length ===1) {
                var bytes = field.buffer();
                return (bytes[0] === 1);
            }
            return( useDefaultTypeCasting() );
        }
    });
}

exports.load = sql => {
    return new Promise((resolve, reject) => {
        console.log("Sql: ",sql);
        const con = createConnection();
        con.connect(err => {
            if (err) reject(err);
        });
        con.query(sql, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
        con.end();
    });
}

exports.add = (tbName, user) => {
    return new Promise((resolve, reject)=>{
        const con = createConnection();
        con.connect(err => {
            if (err) reject(err);
        });

        var sql = `INSERT INTO ${tbName} (Email, Emailconfirmed, PasswordHash, PhoneNumber, Username) VALUES ('${user.Email}', ${user.Emailconfirmed},'${user.PasswordHash}', null, '${user.UserName}')`;
        con.query(sql, function (error, results){
            console.log("sql: ", sql);
            if (error) reject (error);
            resolve(results);
        });
        con.end();
    })
}

exports.addCustomer = (tbName, user) => {
    return new Promise((resolve, reject)=>{
        const con = createConnection();
        con.connect(err => {
            if (err) reject(err);
        });

        var sql = `INSERT INTO customer (Id_Customer, Name_Customer, Address_Customer, Phone, Id_Kind_Of_Custom, Email) VALUES (${user.Id}, '${user.Name}', '${user.Address}', '${user.Phone}', 2, '${user.Email}')`; 
        con.query(sql, function (error, results){
            console.log("sql: ", sql);
            if (error) reject (error);
            console.log("Them vao customer thanh cong");
            resolve(results);
        });
        con.end();
    })
}
