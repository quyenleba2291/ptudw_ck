$(document).ready(function() {
    $("#form").submit(function(e) {
      e.preventDefault();
        let isExistedEmail = await accountM.getByEmail($("#email").val());
        let isExistedUser  = await accountM.getByUsername($("#username").val());
        let couldSubmit = true;
        console.log("Exist email: ", isExistedEmail);
        console.log("Exist user: ", isExistedUser);
        if (isExistedUser !== null) {
            $("#msgUsername").text("Tên đăng nhập đã tồn tại");
            couldSubmit = false;
        } else {
            $("#msgUsername").text("");
        }
        if (isExistedEmail !== null) {
            $("#msgEmail").text("Email đã tồn tại");
            couldSubmit = false;
        } else {
            $("#msgEmail").text("");
        }
        if ($("#password").val() == $("#confirm_password").val()) {
            $("#msgConfirmPassword").text("Mật khẩu nhập lại chưa chính xác");
            couldSubmit = false;
        } else {
            $("#msgConfirmPassword").text("");
        }

    });
});