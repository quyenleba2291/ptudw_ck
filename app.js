var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require('body-parser');
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var indexRouter = require("./controller/Account");
var usersRouter = require("./controller/Bidder/bidderC");
var appAdmin = require("./area/admin/appAdmin");
var appSeller = require("./controller/Seller/sellerC")
var appProduct = require("./controller/Product");
var appSearch = require("./controller/Search");
const session = require("express-session");
var xoauth2 = require("xoauth2");
const accountM = require("./model/Account");
var appCat = require("./controller/Category");
var app = express();
exphbs = require("express-handlebars");
/*config cho express handlebars*/
const hbs = exphbs.create({
  extname: "hbs",
  layoutsDir: __dirname + "/views/layouts/",
  defaultLayout: "layout",
  partialsDir: __dirname + "/views/partials/",
  helpers: {
    add: function(A, B) {
      return A + B;
    }
  }
});
app.use(bodyParser.urlencoded({extended : false}));

app.use(
  session({
    secret: "qweasdzxc",
    resave: true,
    saveUninitialized: true
  })
);
app.engine("hbs", hbs.engine);
// view engine setup
// app.set('views', path.join(__dirname, 'views'));

var rand, mailOptions, link;
app.get("/send", function(req, res) {
  rand=Math.floor((Math.random() * 100) + 54);
  host = req.get("host");
  link = "http://" + req.get("host") + "/verify?id=" + rand;
  mailOptions = {
    to: req.user.Email,
    subject: "SanDauGia - Please confirm OTP Code",
    html:
      "Hello,<br> Please Click on the link to verify your email.<br><a href=" +
      link +
      ">Click here to verify</a>"
  };
  console.log(mailOptions);
  smtpTransport.sendMail(mailOptions, function(error, response) {
    if (error) {
      console.log("Error while sending OTP: ", error);
      res.end("error");
    } else {
      console.log("Message sent: " + response.message);
      res.end("sent");
    }
  });
});

app.use("/sendResult", function(req, res) {
  if (req.session.user) {
    mailOptions = {
      to: req.session.user.Email,
      subject: "SanDauGia - Ra giá thành công",
      html:
        req.session.message
    };
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response) {
      if (error) {
        res.end("error");
      } else {
        console.log("Message sent: " + response.message);
        res.end("sent");
      }
    });
  } else{
    res.redirect('/')
  }
  
});


app.get("/verify", async function(req, res)  {
  // console.log(req.protocol+":/"+req.get('host'));
  if (req.protocol + "://" + req.get("host") == "http://" + host) {
    console.log("Domain is matched. Information is from Authentic email");
    if (req.query.id.trim() === req.session.user.PasswordHash) {
      console.log("email is verified");
      const rs = await accountM.updateEmailConfirm(req.session.user.Email);
      req.session.user.Emailconfirmed = 1;
      res.redirect('/');
    } else {
      console.log("email is not verified");
      res.end("<h1>Bad Request</h1>");
    }
  } else {
    res.end("<h1>Request is from unknown source");
  }
});
app.set("view engine", "hbs");
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/user", indexRouter, usersRouter);
app.use("/admin", appAdmin);
app.use("/category", appCat);
app.use("/product", appProduct);
app.use("/search", appSearch);
app.use("/seller", indexRouter, appSeller);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
